'use strict';

/**
 * @ngdoc function
 * @name showcaseApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the showcaseApp
 */
angular.module('showcaseApp')
  .controller('CvCtrl', function ($scope, $http) {

    $http.get('Data/Cv.JSON').
      success(function(data, status, headers, config) {
        $scope.cvValue = data;
      }).
      error(function(data, status, headers, config) {
        // log error
      });
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
