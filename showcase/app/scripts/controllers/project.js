'use strict';

/**
 * @ngdoc function
 * @name showcaseApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the showcaseApp
 */
angular.module('showcaseApp')
  .controller('ProjectCtrl', function ($scope,$http) {
    $http.get('Data/Project.JSON').
      success(function(data, status, headers, config) {
        $scope.projetValue = data;
      }).
      error(function(data, status, headers, config) {
        // log error
      });
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
