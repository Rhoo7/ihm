'use strict';

/**
 * @ngdoc function
 * @name showcaseApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the showcaseApp
 */
angular.module('showcaseApp')
  .controller('CatCtrl', function ($scope, $http) {
    $http.get('Data/Cat.JSON').
      success(function(data, status, headers, config) {
        $scope.catValue = data;
      }).
      error(function(data, status, headers, config) {
        // log error
      });
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
